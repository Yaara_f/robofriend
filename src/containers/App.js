import React, { Component } from "react";
import CardList from '../components/CardList';
import SearchBox from '../components/SearchBox';
import '../containers/App.css';
import Scroll from '../containers/Scroll';
import ErrorComp from '../components/ErrorComp';


 
 // need state to communicate between thw 2 children- robotList and SearchBox.

class App extends Component{
	constructor(){
		super();
		this.state={
	                robots: [],
					searchField:''
		};

	}

	componentDidMount(){
		fetch('https://jsonplaceholder.typicode.com/users')
			.then(response=> response.json())
			.then(users=>this.setState({robots:users}));
	}

	onSearchChange =(event)=>{
		// can't do this.state.searchField="..." need to use setState()
		this.setState({searchField:event.target.value });
		
	}

	render(){
		const filterRobot=this.state.robots.filter(robot =>{
			return robot.name.toLowerCase().includes(this.state.searchField.toLowerCase());
		})
		if(!this.state.robots.length){
			return <h2 className='tc'> Loading... </h2>
		}else{
			return(
				<div className='tc'>
		  			<h1 className='f1'> RoboFriends </h1>
	      			<SearchBox searchChange={this.state.searchField, this.onSearchChange}/>
	      			<Scroll>
	      				<ErrorComp>
	      					<CardList robots={filterRobot}/>
	      				</ErrorComp>
	      			</Scroll>
	    		</div>
			);
		}
	}
}

export default App;